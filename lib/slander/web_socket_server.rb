require 'bundler/setup'

require 'em-websocket'
require 'eventmachine'

module Slander

  module WebSocketServer

    def run
      EM.epoll
      EM.kqueue

      EM.run do
        options = {
          host:    Slander::Config[:websocket_host],
          port:    Slander::Config[:websocket_port],
          debug:   Slander::Config[:debug],
          app_key: Slander::Config[:app_key]
        }

        if Slander::Config[:tls_options]
          options.merge! secure: true,
            tls_options: Slander::Config[:tls_options]
        end

        EM::WebSocket.start options do |ws|
          # Keep track of handler instance in instance of EM::Connection to ensure a unique handler instance is used per connection.
          ws.class_eval    { attr_accessor :connection_handler }
          # Delegate connection management to handler instance.
          ws.onopen        { |handshake| ws.connection_handler = Slander::Config.socket_handler.new ws, handshake }
          ws.onmessage     { |msg| ws.connection_handler.onmessage msg }
          ws.onclose       { ws.connection_handler.onclose }
        end
      end
    end

    extend self

  end

end

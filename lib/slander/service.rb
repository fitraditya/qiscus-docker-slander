require 'bundler/setup'

require 'rack'
require 'thin'

module Slander

  module Service

    def run
      Slander::Config[:require].each { |f| require f }
      Thin::Logging.silent = false
      Rack::Handler::Thin.run Slander::ApiServer, Host: Slander::Config.api_host, Port: Slander::Config.api_port
      Slander::WebSocketServer.run
    end

    def stop
      EM.stop if EM.reactor_running?
    end

    extend self

    Signal.trap('HUP') { Slander::Service.stop }

  end

end

require 'bundler/setup'

require 'fiber'
require 'em-http-request'

module Slander

  module Webhook

    def post payload
      return unless Slander::Config.webhook_url

      payload = {
        time_ms: Time.now.strftime('%s%L'), events: [payload]
      }.to_json

      digest        = OpenSSL::Digest::SHA256.new
      hmac          = OpenSSL::HMAC.hexdigest(digest, Slander::Config.secret, payload)
      content_type  = 'application/json'

      EM::HttpRequest.new(Slander::Config.webhook_url).
        post(body: payload, head: { 
          "X-Pusher-Key" => Slander::Config.app_key, 
          "X-Pusher-Signature" => hmac, 
          "Content-Type" => content_type 
        })
        # TODO: Exponentially backed off retries for errors
    end

    extend self

  end

end
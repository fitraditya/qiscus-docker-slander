module Slander

  module Config

    def load(opts={})
      options.update opts
    end

    def [](key)
      options[key]
    end

    def options
      @options ||= {
        api_host: '0.0.0.0',
        api_port: '80',
        websocket_host: '0.0.0.0',
        websocket_port: '8080',
        redis_address: 'redis://0.0.0.0:6379/',
        socket_handler: Slander::Handler,
        activity_timeout: 120,
        debug: false,
        require: []
      }
    end

    def method_missing(meth, *args, &blk)
      options[meth]
    end

    extend self

  end

end

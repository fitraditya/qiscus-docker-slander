#!/usr/bin/env ruby

require 'bundler/setup'

require 'rack'
require 'em-hiredis'
require 'eventmachine'
require 'active_support/core_ext/string'
require File.join(File.dirname(__FILE__), 'lib', 'slander', 'version')

options = {
  app_key: ENV['PUSHER_KEY'],
  secret: ENV['PUSHER_SECRET'],
  api_host: '0.0.0.0',
  api_port: 4567,
  websocket_host: '0.0.0.0',
  websocket_port: 8080,
  redis_address: 'redis://' + ENV['QISCUSCORE_REDIS_1_PORT_6379_TCP_ADDR'] + ':' + ENV['QISCUSCORE_REDIS_1_PORT_6379_TCP_PORT'] + '/',
  debug: false
}

module Slander; end

EM.epoll
EM.kqueue

EM.run do
  File.tap do |f|
    Dir[f.expand_path(f.join(f.dirname(__FILE__),'lib', 'slander', '*.rb'))].each do |file|
      Slander.autoload File.basename(file, '.rb').camelize, file
    end
  end

  Slander::Config.load options
  Slander::Service.run

  puts "\n"
  puts "Running Slander v.#{Slander::VERSION}"
  puts "\n"

  puts "Slander API server listening on port #{options[:api_port]}"
  puts "Slander WebSocket server listening on port #{options[:websocket_port]}"
end
